# Nix start

What is Nix? Why would you use it? And how?

## Table of contents

[[_TOC_]]

## Overview / What is Nix?

A language and tools for the software package lifecycle, from packaging to
installing.

## Setting up Nix on Ubuntu

How much work is it actually to get Nix running on Ubuntu? We could use a Docker
container for this, but a virtual machine will be more realistic:

1. Spin up an [Ubuntu image](https://app.vagrantup.com/ubuntu/boxes/jammy64) (or
   open a terminal in WSL if you have that):

   ```bash
   cd /path/to/nix-start
   vagrant up
   vagrant ssh
   ```

1. [Install Nix](https://nixos.org/download.html) (make sure to select the
   relevant platform before installing on your own system):

   ```bash
   sh <(curl -L https://nixos.org/nix/install) --daemon
   ```

   The installer is very verbose and careful by default, but simply answer `y`
   (yes) to everything to go through it quickly. At the end it prompts you to
   restart the shell session:

   > Nix won't work in active shell sessions until you restart them.

1. Reenter the shell:

   ```bash
   exit
   vagrant ssh
   ```

1. Install a package in a temporary shell:

   ```bash
   nix-shell --packages hello
   ```

That command will download GNU Hello and all its dependencies. We can now run
the `hello` command:

```console
[nix-shell:~]$ hello
Hello, world!
```

## What about NixOS?

NixOS is a Linux distribution which uses Nix as the main package manager. It
provides an excellent way to
[configure a system declaratively](https://search.nixos.org/options)
([example](https://gitlab.com/engmark/root/-/blob/9294c74d4b368f709f0e8fc797c7fec504dc283f/configuration.nix)).
But since it's not necessary to do any of the other things in this document,
we'll skip it.

## Finding packages

Now that we have a working Nix environment, we can
[search for Nix packages](https://search.nixos.org/packages)[^package-search] to
install.

[^package-search]:
    Currently the website is the best way to search for packages; the command
    line equivalent —
    `nix --extra-experimental-features 'flakes nix-command' search nixpkgs PACKAGE_NAME`
    — is both clunky and slow.

Let's try installing Python.
[Searching for "python"](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=python),
we immediately see an advantage over most other package repositories: there are
a bunch of different Python versions available for us to
install[^python-versions]! Python 2 is available as the "python2" package for
use with legacy programs, but is
[no longer supported](https://www.python.org/downloads/) by the Python Software
Foundation. So let's instead try the next match, "python39":

> ![](images/python-package-basic.png)

What do we get from the search result?

- The package name.
- A description of the package.
- A name, which I believe is how you can determine which packages provide
  overlapping functionality. This is getting into the technical details, but
  suffice it to say that you can install any number of packages even if they
  have overlapping functionality, because Nix deals with treating only one of
  them as the "main" one. For example, if you have both Python 3.9 and 3.10
  installed, you can check which one is the "main" one by running
  `type -a python` – the first path has the highest priority.
- The full version of the package. The vast majority of the time this is just
  the same version as the upstream package.
- Package
  [outputs](https://nixos.org/manual/nixpkgs/stable/#chap-multiple-output),
  which are variants of the package, such as:
  - the default (`out`),
  - with debug symbols (`debug`), and
  - libraries (`lib`) for building another package.
- A link to the package homepage, useful to verify that it's the package we're
  looking for, or to find the official documentation.
- A link to the package source/definition. This is the Nix file which defines
  how the package is built.
- The package license name as a link to that license.

[^python-versions]:
    As of 4 May 2023, nixpkgs unstable has Python 2, 3.8, 3.9, 3.10, 3.11 and
    3.12, while the official Ubuntu 22.04 repositories have Python 3.10 and 3.11
    (see `sudo apt-get update && apt-cache search python3`).

If you hover over the package and click "Show more package details" there's a
lot more:

> ![](images/python-package-details.png)

- A longer description of the package.
- **Instructions to install the package** using nix-env (_deprecated_), NixOS
  ([not relevant here](#what-about-nixos)), or nix-shell.
- Which programs are provided by the package.
- Info about maintainers. Some older or niche packages might have no official
  maintainer, which can be a useful heuristic.
- Which platforms the package supports.

## Installing packages

Let's try installing the
[Python 3.9 package](https://search.nixos.org/packages?channel=unstable&show=python39&from=0&size=50&sort=relevance&type=packages&query=python39)
we found above. Looking at the package details, we can try the package by just
running `nix-shell -p python39`. After it downloads the package and all its
dependencies, we get into a new shell with the Python 3.9 package. Let's quickly
check the version:

```console
[nix-shell:~]$ python --version
Python 3.9.16

[nix-shell:~]$ type -a python
python is /nix/store/v7h4276ij0sj48qwrf4g4cm3pfc86mmw-python3-3.9.16/bin/python
```

Yep, that's version 3.9.16, installed in the Nix store. Let's exit the Nix shell
and verify that Python is gone:

```console
[nix-shell:~]$ exit
exit
vagrant@ubuntu-jammy:~$ python --version
Command 'python' not found, did you mean:
  command 'python3' from deb python3
  command 'python' from deb python-is-python3
```

It's still in the Nix store, but not currently active. So we can trivially
change to using Python 3.10 instead:

```console
vagrant@ubuntu-jammy:~$ nix-shell -p python310
[downloads files]

[nix-shell:~]$ python --version
Python 3.10.10

[nix-shell:~]$ type -a python
python is /nix/store/fdqpyj613dr0v1l1lrzqhzay7sk4xg87-python3-3.10.10/bin/python
```

No trace of Python 3.9, excellent! Let's try reverting:

```console
[nix-shell:~]$ exit
exit
vagrant@ubuntu-jammy:~$ nix-shell -p python39

[nix-shell:~]$ python --version
Python 3.9.16

[nix-shell:~]$ type -a python
python is /nix/store/v7h4276ij0sj48qwrf4g4cm3pfc86mmw-python3-3.9.16/bin/python
```

This time the Nix shell should have started almost instantly, because the
"python39" package was already in the Nix store.

## Packaging software

[GNU Hello](https://www.gnu.org/software/hello/) is an example package written
in C, which prints "Hello, World!" when running the compiled result. How much is
involved in getting from the upstream tarball to running the `hello` binary?

### Building GNU Hello with Debian/Ubuntu

(Feel free to [skip this section](#building-gnu-hello-with-nix) if you already
know how painful Debian packaging is.)

First, let's look at
[how GNU Hello is packaged in Ubuntu 22.04LTS](https://packages.ubuntu.com/jammy/hello).
We can download the relevant files from the sidebar:

[`hello_2.10.orig.tar.gz`](http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10.orig.tar.gz)
contains the original GNU Hello sources. We'll ignore its contents, since it's
what we're packaging.

[`hello_2.10-2ubuntu4.dsc`](http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.dsc)
contains a bunch of metadata:

```deb822
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: hello
Binary: hello
Architecture: any
Version: 2.10-2ubuntu4
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Homepage: http://www.gnu.org/software/hello/
Standards-Version: 4.3.0
Testsuite: autopkgtest
Build-Depends: debhelper-compat (= 9)
Package-List:
 hello deb devel optional arch=any
Checksums-Sha1:
 f7bebf6f9c62a2295e889f66e05ce9bfaed9ace3 725946 hello_2.10.orig.tar.gz
 ce06556db96050340cc26a5c5b563bdc57d46f0e 6832 hello_2.10-2ubuntu4.debian.tar.xz
Checksums-Sha256:
 31e066137a962676e89f69d1b65382de95a7ef7d914b8cb956f41ea72e0f516b 725946 hello_2.10.orig.tar.gz
 1fd3b7bdee98c1314a43700f42fc0df8a06e194eda40117d096c8e26f5198a8a 6832 hello_2.10-2ubuntu4.debian.tar.xz
Files:
 6cd0ffea3884a4e79330338dcc2987d6 725946 hello_2.10.orig.tar.gz
 43f8fd6bd1aef52786beb6e171368ecf 6832 hello_2.10-2ubuntu4.debian.tar.xz
Original-Maintainer: Santiago Vila <sanvila@debian.org>

-----BEGIN PGP SIGNATURE-----

iQEzBAEBCgAdFiEEqx+XcX7ftBm4bj5/AhnKGdA0MwwFAmI7J1gACgkQAhnKGdA0
MwzXIQgAkmOdM1IKmE7R/mLSPQuTR0NI1h0/8glf9sgi5mX3EgBvyreDumNXLJK7
S4KV1JLZ1pd6fmwxFFm5jwWve5R5Jb1EUt5bJFUtWVQtz2E9G9CeWPuzKpXdHx44
OcpB5dk10XZCSHiNTy6o9FvdKfeqNyUJK19HnvmBp2XFgs4QgXNpmU9Ys+4NrMDE
H3/3JOaqPtAYU9l2yi9txQfzr778nJ3/vwOWXqpDbeSi7NCjb4/6dkv1i7b4JOD4
mN3ejf6lZxq3rcNCFKRFIujwVH52YcEci5CkCAzKD1EMgd4Nb0GfJ1Hqaen6TDQL
E8eh2tpBiKkVcIhDbYdT4Bf/rGFzqQ==
=x/WD
-----END PGP SIGNATURE-----
```

[`hello_2.10-2ubuntu4.debian.tar.xz`](http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.debian.tar.xz)
contains another eight files in six separate formats, duplicating some of the
information in `hello_2.10-2ubuntu4.dsc`:

- `debian/control` 'contains various values which **dpkg**, **dselect**,
  **apt-get**, **apt-cache**, **aptitude**, and other package management tools
  will use to manage the package. It is defined by the
  [Debian Policy Manual, 5 "Control files and their fields"](http://www.debian.org/doc/debian-policy/ch-controlfields.html).'
  So that's one format and at least five tools to learn.
- `debian/tests/control`, a separate `control` file for tests.
- `debian/changelog`, a change log with a
  [custom format](https://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog).
- `debian/copyright`, basically a copy of the upstream license in a
  [custom format](http://dep.debian.net/deps/dep5/).
- `debian/rules`, a
  ["**Makefile**, but different from the one(s) in the upstream source."](https://www.debian.org/doc/manuals/maint-guide/dreq.en.html#rules)
- `debian/rules-old`, another Makefile with an unknown purpose, other than that
  it's to do with some older syntax. From the file itself: "See debian/rules if
  you prefer the new dh syntax."
- `debian/source/format`,
  ["a single line indicating the desired format for the source package"](https://www.debian.org/doc/manuals/maint-guide/dother.en.html#sourcef).
  Not sure why that's needed.
- `debian/watch`
  ["configures the **uscan** program […] to watch the site where you originally got the source."](https://www.debian.org/doc/manuals/maint-guide/dother.en.html#watch)

At least four of these files are
[required](https://www.debian.org/doc/manuals/maint-guide/dreq.en.html). Let's
try building this in a Docker container:

1. Start the Docker container:

   ```bash
   docker run --interactive --rm --tty ubuntu:22.04
   ```

1. Go to a new build directory:

   ```bash
   cd "$(mktemp --directory)"
   ```

1. Get the files:

   ```bash
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10.orig.tar.gz
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.dsc
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.debian.tar.xz
   ```

   That results in the error

   > bash: wget: command not found

1. The Ubuntu container is pretty minimal, which is good. Let's install `wget`
   and retry:

   ```bash
   apt-get update
   apt-get install -y wget
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10.orig.tar.gz
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.dsc
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.debian.tar.xz
   ```

1. Just try building:

   ```bash
   dpkg-buildpackage
   ```

   > bash: dpkg-buildpackage: command not found

1. Let's figure out which package it is, install it and retry:

   ```bash
   apt-get install -y dpkg-dev
   dpkg-buildpackage
   ```

   > dpkg-buildpackage: error: cannot open file debian/changelog: No such file
   > or directory

1. Right, `dpkg-buildpackage` doesn't read `hello_2.10-2ubuntu4.debian.tar.xz`;
   we need to unpack it before retrying:

   ```bash
   tar -xf hello_2.10-2ubuntu4.debian.tar.xz
   dpkg-buildpackage
   ```

   > dpkg-checkbuilddeps: error: Unmet build dependencies: debhelper-compat
   > (= 9)

1. Rinse and retry:

   ```bash
   apt-get install -y debhelper-compat
   dpkg-buildpackage
   ```

   > dpkg-source: error: can't build with source format '3.0 (quilt)': no
   > upstream tarball found at ../hello_2.10.orig.tar.{bz2,gz,lzma,xz}

1. Wait, it requires the upstream tarball to be in the _parent_ directory?
   Alright, let's fix that and retry:

   ```bash
   mkdir hello_2.10-2ubuntu4
   mv debian hello_2.10-2ubuntu4
   cd hello_2.10-2ubuntu4
   dpkg-buildpackage
   ```

   > dh_installdocs: error: cp --reflink=auto -a NEWS
   > debian/hello/usr/share/doc/hello returned exit code 1

   At this point I'm stumped. The `dpkg-buildpackage` output mentions a `NEWS`
   file, but in a weird way:

   > dpkg-source: warning: ignoring deletion of file NEWS, use --include-removal
   > to override

1. What does that mean? Side quest time! Looking at the start of
   `dpkg-buildpackage --help`, and considering that we're trying to build a
   source package, why don't we try a source-only build instead of a "full"
   build?

   ```bash
   dpkg-buildpackage --build=source
   ```

   Despite a lot of warnings, that finished with an exit code of 0. But it
   didn't create a `.deb` file. Wat.

   Should "ignoring deletion of file NEWS" actually be read as "couldn't find
   file NEWS"? The build output does include

   > dpkg-source: info: building hello using existing ./hello_2.10.orig.tar.gz

   Surely that means the build tool is going to do the unpacking itself, right?
   Let's try unpacking the upstream sources and retrying:

   ```bash
   cd ..
   tar -xf hello_2.10.orig.tar.gz
   cd -
   dpkg-buildpackage
   ```

   That fails in the same way as above, with a missing `NEWS` file. Should the
   source be unpacked in the same directory as the package tarball, overwriting
   any duplicate paths?

   ```bash
   tar -xf ../hello_2.10.orig.tar.gz
   dpkg-buildpackage
   ```

   > dpkg-source: warning: ignoring deletion of file NEWS, use --include-removal
   > to override dpkg-source: error: unrepresentable changes to source

1. It looks like I've messed up the build directory. At this point I had to
   [ask online](https://unix.stackexchange.com/q/743101/3645). Let's recap the
   basic setup in a new `docker run --interactive --rm --tty ubuntu:22.04`
   instance:

   ```bash
   cd "$(mktemp --directory)"
   apt-get update
   apt-get install -y debhelper-compat dpkg-dev wget
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10.orig.tar.gz
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.dsc
   wget http://archive.ubuntu.com/ubuntu/pool/main/h/hello/hello_2.10-2ubuntu4.debian.tar.xz
   ```

1. [Continue according to Stephen Kitt's answer](https://unix.stackexchange.com/a/743117/3645):

   ```bash
   tar xf hello_2.10.orig.tar.gz
   cd hello*/
   tar xf ../hello_2.10-2ubuntu4.debian.tar.xz
   dpkg-buildpackage
   ```

1. Install:

   ```bash
   dpkg -i ../hello_2.10-2ubuntu4_amd64.deb
   ```

1. Run the resulting binary:

   ```bash
   hello
   ```

And done!

### Building GNU Hello with Nix

Let's compare with Nix. We use the
[package search](https://search.nixos.org/packages?type=packages&query=hello) to
determine that the GNU Hello package is defined in
[two Nix files](https://github.com/NixOS/nixpkgs/tree/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello),
[`default.nix`](https://github.com/NixOS/nixpkgs/blob/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello/default.nix)
and
[`test.nix`](https://github.com/NixOS/nixpkgs/blob/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello/test.nix),
both in the Nix language.

Let's try building GNU Hello in a Nix container:

1.  Start the Docker container:

    ```bash
    docker run --interactive --rm --tty nixos/nix:2.14.1
    ```

1.  Go to a new build directory:

    ```bash
    cd "$(mktemp --directory)"
    ```

1.  Get the files:

    ```bash
    wget https://raw.githubusercontent.com/NixOS/nixpkgs/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello/default.nix
    wget https://raw.githubusercontent.com/NixOS/nixpkgs/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello/test.nix
    ```

1.  Just try building:

    ```bash
    nix-build
    ```

    > error: cannot evaluate a function that has an argument without a value
    > ('fetchurl') Nix attempted to evaluate a function as a top level
    > expression; in this case it must have its arguments supplied either by
    > default values, or passed explicitly with '--arg' or '--argstr'. See
    > https://nixos.org/manual/nix/stable/language/constructs.html#functions.
    >
    > at /tmp/tmp.NKdlKFwSQY/default.nix:4:3:
    >
    >         3| , stdenv
    >         4| , fetchurl
    >          |   ^
    >         5| , nixos

    A quick search for the start of that error message found an
    [explanation](https://discourse.nixos.org/t/cannot-evaluate-a-function-that-has-an-argument-without-a-value-runcommand/22619/2):

    > This is not intended to be built with a raw `nix-build`, it’s supposed to
    > be invoked with nixpkgs’ callPackage. Instead you could use:
    >
    > ```bash
    > nix-build -E 'let pkgs = import <nixpkgs> { }; in pkgs.callPackage ./docbook.nix {}'
    > ```

1.  Let's try that after replacing `docbook.nix` with `default.nix`:

    ```bash
    nix-build -E 'let pkgs = import <nixpkgs> { }; in pkgs.callPackage ./default.nix {}'
    ```

1.  Run the resulting binary:

    ```bash
    result/bin/hello
    ```

And done! In summary:

- Once we got into a temporary directory within the Docker container, we needed
  _two tools_ to build and install the package with Nix: `wget` to get the
  package definitions and `nix-build` to build it in a `result` directory. We
  needed _six tools_ to build and install with Ubuntu: `apt-get` to install
  build dependencies, `wget` to get the package definitions and upstream
  sources, `tar` and `cd` to create a buildable directory structure,
  `dpkg-buildpackage` to build and `dpkg` to install.
  - It is possible to
    [do the Ubuntu build with only three tools](https://unix.stackexchange.com/a/743117/3645),
    but that requires more manual configuration plus `dpkg` to install the
    result, basically bringing us to five tools (including the configuration
    editor).
- The Nix package definitions included _53 code lines and no other
  configuration, in two files and a single language._ The Ubuntu package
  definition included _55 code lines_ (`debian/rules*`) and _74 configuration
  lines_ (excluding `debian/changelog`, `debian/copyright`, and `debian/rules*`)
  in _nine files,_ using _five different languages._
- Nix build dependencies were pulled in based on the package definition, while
  Ubuntu build dependencies had to be installed manually.

## Package contents

What's in a Nix package? Let's enter a new Nix shell with `nix-shell -p hello`
and get the path of the `hello` program with `type -p hello`. That will print
the full path of the `hello` executable, which will be something like
`/nix/store/vd7m25a2r2v96ir53nrk8yxv73lnzc9s-hello-2.12.1/bin/hello`. The
`/nix/store/vd7m25a2r2v96ir53nrk8yxv73lnzc9s-hello-2.12.1` part is the
[store path](https://zero-to-nix.com/concepts/nix-store#store-paths), where the
contents unique to that package is stored. Let's have a quick look at what's
there:

```console
[nix-shell:~]$ ls --classify -l /nix/store/vd7m25a2r2v96ir53nrk8yxv73lnzc9s-hello-2.12.1
total 8
dr-xr-xr-x 2 root root 4096 Jan  1  1970 bin/
dr-xr-xr-x 5 root root 4096 Jan  1  1970 share/
```

Some interesting things to note about these:

- All the files have a fixed modification time, which is necessary to have truly
  reproducible packages.
- The files are owned by root, even though we triggered the installation as a
  normal user. That's because the `nix-shell` command told the Nix daemon to do
  the build, and the Nix daemon runs as root.
- The files are not writeable by root. This is a safety net to avoid you
  modifying any of the files in the Nix store manually.

Going one level deeper, the `bin` directory just contains the `hello`
executable, as we already saw. The `share` directory contains three directories:

- `info` has the contents for comprehensive info about the command; see
  `info hello`.
- `locale` has localisation for the command. The Ubuntu VM comes with only
  barebones locale support, so we have to configure the locale we want to use
  before we can localise the command:

  ```bash
  sudo dpkg-reconfigure locales
  # Add the locale you want to use, in this case "nb_NO.UTF-8 UTF-8"
  nix-shell -p hello
  export LOCALE_ARCHIVE=/usr/lib/locale/locale-archive
  LANG=nb_NO.UTF-8 hello
  ```

  This prints "Hei, verden!", the Norwegian Bokmål equivalent of "Hello,
  world!".

- `man` has the contents for the manual page; see `man hello`.

At this level the package only differs from the Ubuntu equivalent in some
details (see `dpkg-query -L hello`), so we can stop digging.

## Nix language

tl;dr: It's a
[pure, functional, lazy, dynamically typed, declarative, packaging-specific language](https://nixos.org/manual/nix/stable/language/index.html).

The "packaging-specific" part is just to say that the language was designed only
to do packaging stuff. So even though it's Turing-complete, it's not recommended
for any other type of programming.

Let's explore the Nix language a bit using the built-in
[Nix REPL](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-repl.html).
If we run `nix repl` we'll see a new prompt where we can run Nix expressions
directly:

```console
vagrant@ubuntu-jammy:~$ nix repl
Welcome to Nix 2.15.0. Type :? for help.

nix-repl>
```

That `:?` command is useful to explain the REPL syntax:

```nix-repl
nix-repl> :?
The following commands are available:

  <expr>        Evaluate and print expression
  <x> = <expr>  Bind expression to variable
  :a <expr>     Add attributes from resulting set to scope
  :b <expr>     Build a derivation
  :bl <expr>    Build a derivation, creating GC roots in the working directory
  :e <expr>     Open package or function in $EDITOR
  :i <expr>     Build derivation, then install result into current profile
  :l <path>     Load Nix expression and add it to scope
  :lf <ref>     Load Nix flake and add it to scope
  :p <expr>     Evaluate and print expression recursively
  :q            Exit nix-repl
  :r            Reload all files
  :sh <expr>    Build dependencies of derivation, then start nix-shell
  :t <expr>     Describe result of evaluation
  :u <expr>     Build derivation, then start nix-shell
  :doc <expr>   Show documentation of a builtin function
  :log <expr>   Show logs for a derivation
  :te [bool]    Enable, disable or toggle showing traces for errors
```

"Expression" and "variable" above refers to Nix code. The ":"-prefixed commands
are all specific to the REPL. Press <kbd>Tab</kbd> to get a list of available
Nix functions, or to generally auto-complete a word. Press <kbd>Ctrl+r</kbd> to
search backwards through your commands for a string.

(There is some overlap between what's available at the top level and what's
within `builtins`, but it's not clear why. `dirOf != builtins.dirOf`, for
example, but `:doc dirOf` is identical to `:doc builtins.dirOf`, so the
top-level functions are probably just a convenience to avoid prefixing
everything with `builtins.`.)

By default the Nix REPL has no knowledge of the Nix package repository; it's
just the Nix language itself. You can load the currently active nixpkgs version
into a variable:

```nix-repl
pkgs = import <nixpkgs> {}
```

This assigns the whole nixpkgs [derivation](#vocabulary) to the variable `pkgs`,
without evaluating any of the packages. We can then use the `:p` REPL function
to force recursive evaluation and print some results, for example the Nix
package maintainers (formatted for readability):

```nix-repl
nix-repl> :p pkgs.nix.meta.maintainers
[
  {
    email = "edolstra+nixpkgs@gmail.com";
    github = "edolstra";
    githubId = 1148549;
    name = "Eelco Dolstra";
  }
  {
    email = "meurerbernardo@gmail.com";
    github = "lovesegfault";
    githubId = 7243783;
    keys = [
      {
        fingerprint = "F193 7596 57D5 6DA4 CCD4  786B F4C0 D53B 8D14 C246";
      }
    ];
    matrix = "@lovesegfault:matrix.org";
    name = "Bernardo Meurer";
  }
  {
    email = "artturin@artturin.com";
    github = "Artturin";
    githubId = 56650223;
    matrix = "@artturin:matrix.org";
    name = "Artturi N";
  }
]
```

As you can see, this was pretty much instantaneous. Because Nix is lazily
evaluated, it only had to look up a handful of values to produce the result
above, rather than having to build the whole package first. This also means we
can do some nifty stuff pretty quickly, like finding all the packages maintained
by Eelco Dolstra, the inventor of Nix. Let's start by creating a simple function
to check if a package has a maintainer with the right name:

1. Find
   [the nix package](https://search.nixos.org/packages?channel=unstable&show=nix&from=0&size=50&sort=relevance&type=packages&query=nix)
1. Go to the
   [source](https://github.com/NixOS/nixpkgs/blob/nixos-22.11/pkgs/tools/package-management/nix/common.nix#L215)
1. On the
   [`maintainers` line](https://github.com/NixOS/nixpkgs/blob/cd749f58ba83f7155b7062dd49d08e5e47e44d50/pkgs/tools/package-management/nix/common.nix#L225)
   we get a hint that `eelco` is the user we want. But where does that variable
   come from? `with maintainers` earlier on the line is equivalent to Python's
   `from maintainers import *` and Java's `import maintainers.*`, where all the
   entries _within_ `maintainers` becomes a top-level variable.
   [This can make it difficult to work out where it comes from](https://nix.dev/anti-patterns/language#with-attrset-expression),
   but fortunately the code here is pretty straight-forward: `eelco` is really
   `maintainers.eelco`, which is really `lib.maintainers.eelco`. `lib` is passed
   into this package as a parameter, but it also happens to be a top-level
   attribute of nixpkgs[^parameter-tradeoff]. Since we've imported nixpkgs as
   `pkgs` the full reference is `pkgs.lib.maintainers.eelco`:

   ```nix-repl
   nix-repl> pkgs.lib.maintainers.eelco
   { email = "edolstra+nixpkgs@gmail.com"; github = "edolstra"; githubId = 1148549; name = "Eelco Dolstra"; }
   ```

   Cool!

1. [`:doc builtins.elem`](https://nixos.org/manual/nix/stable/language/builtins.html#builtins-elem)
   shows that this is the function we want to use to check whether an element is
   in a list. Let's assert that Eelco Dolstra maintains `pkgs.nix`:

   ```nix-repl
   nix-repl> builtins.elem pkgs.lib.maintainers.eelco pkgs.nix.meta.maintainers
   true
   ```

   Oh yeah,
   [function application with multiple arguments is done by currying](https://nix.dev/tutorials/nix-language#multiple-arguments),
   a common approach in functional languages. The above is equivalent to
   `(builtins.elem pkgs.lib.maintainers.eelco) pkgs.nix.meta.maintainers`, and
   you only need parentheses when it's required to disambiguate expressions.

1. The currying approach means we can easily create a function to check whether
   Eelco Dolstra maintains _any_ package:

   ```nix-repl
   nix-repl> eelcoMaintains = pkg: builtins.elem pkgs.lib.maintainers.eelco pkg.meta.maintainers

   nix-repl> eelcoMaintains pkgs.nix
   true
   ```

1. [`:doc builtins.filter`](https://nixos.org/manual/nix/stable/language/builtins.html#builtins-filter)
   shows that we can use this function to get a list of elements matching a
   filter function. We can use `eelcoMaintains` as that filter function! Let's
   try running it over `pkgs` directly:

   ```nix-repl
   nix-repl> builtins.filter eelcoMaintains pkgs
   error:
          … while calling the 'filter' builtin

            at «string»:1:1:

               1| builtins.filter eelcoMaintains pkgs
                | ^

          … while evaluating the second argument passed to builtins.filter

            at «none»:0: (source not available)

          error: value is a set while a list was expected
   ```

1. Of course, `pkgs` is an [attribute set](#vocabulary), a mapping of key/value
   pairs, so we need to get the values of the attribute set first. This is
   dynamic typing in action.
   [`:doc builtins.attrValues`](https://nixos.org/manual/nix/stable/language/builtins.html#builtins-attrValues)
   shows that this is what we need, so let's try that:

   ```nix-repl
   nix-repl> builtins.filter eelcoMaintains builtins.attrValues pkgs
   error:
          … while calling the 'filter' builtin

            at «string»:1:1:

               1| builtins.filter eelcoMaintains builtins.attrValues pkgs
                | ^

          … while evaluating the second argument passed to builtins.filter

            at «none»:0: (source not available)

          error: value is the built-in function 'attrValues' while a list was expected
   ```

   Oh, whoops, remember the function application above?
   `builtins.filter eelcoMaintains builtins.attrValues pkgs` is equivalent to
   `((builtins.filter eelcoMaintains) builtins.attrValues) pkgs`, so we're
   trying to filter the `attrValues` function rather than the list of packages.

1. Let's explicitly override the precedence:

   ```nix-repl
   nix-repl> builtins.filter eelcoMaintains (builtins.attrValues pkgs)
   error:
          … while calling the 'filter' builtin

            at «string»:1:1:

               1| builtins.filter eelcoMaintains (builtins.attrValues pkgs)
                | ^

          … while calling the 'elem' builtin

            at «string»:1:7:

               1|  pkg: builtins.elem pkgs.lib.maintainers.eelco pkg.meta.maintainers
                |       ^

          (stack trace truncated; use '--show-trace' to show the full trace)

          error: Please be informed that this pseudo-package is not the only part of
          Nixpkgs that fails to evaluate. You should not evaluate entire Nixpkgs
          without some special measures to handle failing packages, like those taken
          by Hydra.
   ```

1. At this point I
   [asked the community for help](https://discourse.nixos.org/t/how-to-filter-nixpkgs-by-metadata/27473).
   That taught me a few things:

   - Nixpkgs has safeguards to make sure you don't evaluate all 80k+ packages
     unless you know what you're doing, and that's what the error message above
     is about. Since I don't know what I'm doing, we'll give it a try anyway by
     just ignoring errors!
   - We should wrap the `eelcoMaintains` invocation in
     [`builtins.tryEval`](https://nixos.org/manual/nix/stable/language/builtins.html#builtins-tryEval).
     That way we can simply treat any broken packages as not maintained by Eelco
     Dolstra. A more robust solution would probably do something more clever,
     but we don't need that for this demonstration.
   - There's a `pkgs.lib.filterAttrs` which we can use to filter the packages
     attribute set directly rather than using only the list of package
     definitions.
   - Some packages are nested inside attributes to group them, such as
     `pkgs.python3Packages.*`, but we'll ignore those for simplicity.
   - Using `builtins.attrNames` on the result I can get the package name you'd
     put in `nix-shell -p …` instead of the name within the package definition.
     That is, for the Python 3.9 package we looked at above we should get
     "python39" rather than just "python".

   The result, after a bit of fiddling, is this:

   ```nix-repl
   pkgs = import <nixpkgs> {}
   pkgMaintainers = pkg: pkg.meta.maintainers or []
   eelcoMaintains = pkg: builtins.elem pkgs.lib.maintainers.eelco (pkgMaintainers pkg)
   robustEelcoMaintains = pkg:
     let
       result = builtins.tryEval (eelcoMaintains pkg);
     in
       if result.success
       then result.value
       else false
   eelcoMaintainsPackages = pkgs.lib.filterAttrs (_name: pkg: robustEelcoMaintains pkg) pkgs
   builtins.attrNames eelcoMaintainsPackages
   ```

   In a few seconds this evaluates the relevant parts of about 2.4 million lines
   of Nix code defining 80,000+ packages in nixpkgs to produce a list of 134
   package names, including "nix", the one we started with. You'll also notice
   that only the last line actually takes a long time, because the variable
   assignments aren't evaluated until we print the result in the final line.
   This is lazy evaluation in practice.

[^parameter-tradeoff]:
    Nixpkgs is huge, so passing things like `lib` to a package is a simple way
    to be able to write `lib.[…]` instead of `pkgs.lib.[…]`. When doing your own
    packaging, you might want to add a bit of verbosity and pass `pkgs` instead,
    for clarity.

One thing we didn't explore above is Nix purity. _Pure_ means a piece of Nix
code always produces the same result. In other words,
[only the build inputs are available to the package build](https://youtu.be/pfIDYQ36X0k?t=486),
and any inputs fetched from a network must be defined with a matching checksum.
Let's have a look at a real example within the Nix command line itself.

For example, let's have a look at how the
[GNU Hello package definition](https://raw.githubusercontent.com/NixOS/nixpkgs/115a96e2ac1e92937cd47c30e073e16dcaaf6247/pkgs/applications/misc/hello/default.nix)
gets the upstream package sources (I've inlined a variable for clarity):

```nix-repl
nix-repl> pkgs.fetchurl {
            url = "mirror://gnu/hello/hello-2.12.1.tar.gz";
            sha256 = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
          }
«derivation /nix/store/1iad0162c97snn22vgby4xa7fbpi3i17-hello-2.12.1.tar.gz.drv»
```

What happens if we remove the checksum?

```nix-repl
nix-repl> pkgs.fetchurl {
            url = "mirror://gnu/hello/hello-2.12.1.tar.gz";
          }
warning: found empty hash, assuming 'sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
«derivation /nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv»
```

Turns out the Nix REPL runs in impure mode by default! It's just a Nix language
playground after all. Let's try actually putting this in a Nix file and building
that:

```console
vagrant@ubuntu-jammy:~$ cat > default.nix <<'EOF'
> let
>   pkgs = import <nixpkgs> {};
> in
> pkgs.fetchurl {
>   url = "mirror://gnu/hello/hello-2.12.1.tar.gz";
> }
> EOF
vagrant@ubuntu-jammy:~$ nix-build
warning: found empty hash, assuming 'sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='
this derivation will be built:
  /nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv
building '/nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv'...
[…]
trying https://ftpmirror.gnu.org/hello/hello-2.12.1.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:--  0:00:06 --:--:--     0
100 1009k  100 1009k    0     0   125k      0  0:00:08  0:00:08 --:--:--  644k
error: hash mismatch in fixed-output derivation '/nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv':
         specified: sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=
            got:    sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=
```

Reading the output from top to bottom:

> warning: found empty hash, assuming
> 'sha256-AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA='

Basically, `pkgs.fetchurl` will use an obviously-wrong default value, so that
any subsequent code which relies on a correct hash will fail _eventually._

The next three lines basically just say that `nix-build` has created an
intermediate
[derivation file](https://nixos.org/guides/nix-pills/our-first-derivation.html#idm140737320419584)
which it will build. We can inspect this file, to just get an idea for what goes
into a trivial build:

```nix-repl
vagrant@ubuntu-jammy:~$ nix --extra-experimental-features nix-command derivation show /nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv
warning: The interpretation of store paths arguments ending in `.drv` recently changed. If this command is now failing try again with '/nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv!*'
{
  "/nix/store/vnlan6a06gf62rybdz3bm4sxb35yy0gg-hello-2.12.1.tar.gz.drv": {
    "args": [
      "-e",
      "/nix/store/57620l1168piiia2bmmsxxhh7sjb2n40-builder.sh"
    ],
    "builder": "/nix/store/zlf0f88vj30sc7567b80l52d19pbdmy2-bash-5.2-p15/bin/bash",
    "env": {
      "SSL_CERT_FILE": "/nix/store/8yc3bj1an27xdazw3m1q092aksi0aqv5-nss-cacert-3.86/etc/ssl/certs/ca-bundle.crt",
      "__structuredAttrs": "",
      "buildInputs": "",
      "builder": "/nix/store/zlf0f88vj30sc7567b80l52d19pbdmy2-bash-5.2-p15/bin/bash",
      "cmakeFlags": "",
      "configureFlags": "",
      "curlOpts": "",
      "curlOptsList": "",
      "depsBuildBuild": "",
      "depsBuildBuildPropagated": "",
      "depsBuildTarget": "",
      "depsBuildTargetPropagated": "",
      "depsHostHost": "",
      "depsHostHostPropagated": "",
      "depsTargetTarget": "",
      "depsTargetTargetPropagated": "",
      "doCheck": "",
      "doInstallCheck": "",
      "downloadToTemp": "",
      "executable": "",
      "impureEnvVars": "http_proxy https_proxy ftp_proxy all_proxy no_proxy NIX_CURL_FLAGS NIX_HASHED_MIRRORS NIX_CONNECT_TIMEOUT NIX_MIRRORS_alsa NIX_MIRRORS_apache NIX_MIRRORS_bioc NIX_MIRRORS_bitlbee NIX_MIRRORS_centos NIX_MIRRORS_cpan NIX_MIRRORS_cran NIX_MIRRORS_debian NIX_MIRRORS_fedora NIX_MIRRORS_gcc NIX_MIRRORS_gentoo NIX_MIRRORS_gnome NIX_MIRRORS_gnu NIX_MIRRORS_gnupg NIX_MIRRORS_hackage NIX_MIRRORS_hashedMirrors NIX_MIRRORS_ibiblioPubLinux NIX_MIRRORS_imagemagick NIX_MIRRORS_kde NIX_MIRRORS_kernel NIX_MIRRORS_luarocks NIX_MIRRORS_maven NIX_MIRRORS_mozilla NIX_MIRRORS_mysql NIX_MIRRORS_openbsd NIX_MIRRORS_opensuse NIX_MIRRORS_osdn NIX_MIRRORS_postgresql NIX_MIRRORS_pypi NIX_MIRRORS_qt NIX_MIRRORS_roy NIX_MIRRORS_sageupstream NIX_MIRRORS_samba NIX_MIRRORS_savannah NIX_MIRRORS_sourceforge NIX_MIRRORS_steamrt NIX_MIRRORS_tcsh NIX_MIRRORS_testpypi NIX_MIRRORS_ubuntu NIX_MIRRORS_xfce NIX_MIRRORS_xorg",
      "mesonFlags": "",
      "mirrorsFile": "/nix/store/88f2m3n8x4q5f59wnk1jsv987glhc19q-mirrors-list",
      "name": "hello-2.12.1.tar.gz",
      "nativeBuildInputs": "/nix/store/9l2b851fyp1f2dvs4h7xfc0159s000nn-curl-8.0.1-dev",
      "nixpkgsVersion": "23.05",
      "out": "/nix/store/ipz0f8vd7a1p5sz64gxlgf7dd7rbbj5d-hello-2.12.1.tar.gz",
      "outputHash": "",
      "outputHashAlgo": "sha256",
      "outputHashMode": "flat",
      "outputs": "out",
      "patches": "",
      "postFetch": "",
      "preferHashedMirrors": "1",
      "preferLocalBuild": "1",
      "propagatedBuildInputs": "",
      "propagatedNativeBuildInputs": "",
      "showURLs": "",
      "stdenv": "/nix/store/aa283g93zqf3111m66kawl6d5z3wlawd-stdenv-linux",
      "strictDeps": "",
      "system": "x86_64-linux",
      "urls": "mirror://gnu/hello/hello-2.12.1.tar.gz"
    },
    "inputDrvs": {
      "/nix/store/70mzivwc9zl1p41qp6dm8aac81achxip-stdenv-linux.drv": [
        "out"
      ],
      "/nix/store/f5l0pmhipbbr6y7nblmyqrxx7va7x33m-nss-cacert-3.86.drv": [
        "out"
      ],
      "/nix/store/i274dr0pk4gfix29xklf7g5a83g8aiig-mirrors-list.drv": [
        "out"
      ],
      "/nix/store/lima2l2cfn9qacxzq0p2b7k8bfs94n1m-curl-8.0.1.drv": [
        "dev"
      ],
      "/nix/store/q0ml0xhn7vlranc9h2gh7kwy357jmr4h-bash-5.2-p15.drv": [
        "out"
      ]
    },
    "inputSrcs": [
      "/nix/store/57620l1168piiia2bmmsxxhh7sjb2n40-builder.sh"
    ],
    "name": "hello-2.12.1.tar.gz",
    "outputs": {
      "out": {
        "hash": "0000000000000000000000000000000000000000000000000000000000000000",
        "hashAlgo": "sha256",
        "path": "/nix/store/ipz0f8vd7a1p5sz64gxlgf7dd7rbbj5d-hello-2.12.1.tar.gz"
      }
    },
    "system": "x86_64-linux"
  }
}
```

There's a lot of stuff here, but notably absent is the entirety of nixpkgs!
There's a reference to the URL we specified, to `bash` to run the builder
script, and to the dependencies of the fetcher for this type of URL (CA
certificates for TLS, the mirror list, and cURL to actually download the file).
You could copy the `.nix` file to another machine and produce the exact same
`.drv` files and subsequent results.

Near the end is the cURL output, which is just a standard progress report.

Finally, `nix-build` checks the hash of the file it's downloaded, and stops
because it doesn't match. At this point, all we need to do to fix the build is
to add a `hash = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";` line to
`default.nix` (assuming we trust that the file we've downloaded is the right
one), so it looks like this:

```nix
let
  pkgs = import <nixpkgs> {};
in
pkgs.fetchurl {
  url = "mirror://gnu/hello/hello-2.12.1.tar.gz";
  hash = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
}
```

Let's retry the build:

```nix-repl
vagrant@ubuntu-jammy:~$ nix-build
/nix/store/pa10z4ngm0g83kx9mssrqzz30s84vq7k-hello-2.12.1.tar.gz
```

Nice! That is the file we described as the output of `default.nix`. We can
manually verify the checksum[^sha256-format]:

```nix-repl
vagrant@ubuntu-jammy:~$ openssl sha256 -binary /nix/store/pa10z4ngm0g83kx9mssrqzz30s84vq7k-hello-2.12.1.tar.gz | base64
jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=
```

[^sha256-format]:
    The simpler `sha256sum` command unfortunately doesn't support base64 encoded
    output formatting.

At this point you might've noticed another source of non-reproducibility: the
version of nixpkgs! We deal with this by
["pinning" the version of nixpkgs](https://nix.dev/tutorials/towards-reproducibility-pinning-nixpkgs).
We'll have a look at manual pinning to understand it, but have a look at
[Niv](https://github.com/nmattia/niv) or
[flakes](https://nixos.wiki/wiki/Flakes) for dealing with this automatically.

To pin nixpkgs manually we replace `pkgs = import <nixpkgs> {};` with the
following template:

```nix
pkgs =
  import (
    fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/GIT_COMMIT_ID.tar.gz";
      sha256 = "HASH";
    }
  )
  {};
```

Run `cat /nix/var/nix/profiles/per-user/root/channels/nixpkgs/.git-revision` to
get the Git commit ID of current nixpkgs, then run
`nix-prefetch-url --type sha256 --unpack "https://github.com/nixos/nixpkgs/archive/$(cat /nix/var/nix/profiles/per-user/root/channels/nixpkgs/.git-revision).tar.gz"`
to get the SHA-256 hash of the nixpkgs tarball. Filling these into the template
above we get this:

```nix
pkgs =
  import (
    fetchTarball {
      url = "https://github.com/NixOS/nixpkgs/archive/0a4206a51b386e5cda731e8ac78d76ad924c7125.tar.gz";
      sha256 = "0gjlg8gwsgfm3gzwg5mzg4nh8lhlid18ls6barvahllbjp6sdc9j";
    }
  )
  {};
```

After replacing the `pkgs = import <nixpkgs> {};` line in `default.nix` we end
up with this:

```nix
let
  pkgs =
    import (
      fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/0a4206a51b386e5cda731e8ac78d76ad924c7125.tar.gz";
        sha256 = "0gjlg8gwsgfm3gzwg5mzg4nh8lhlid18ls6barvahllbjp6sdc9j";
      }
    )
    {};
in
  pkgs.fetchurl {
    url = "mirror://gnu/hello/hello-2.12.1.tar.gz";
    hash = "sha256-jZkUKv2SV28wsM18tCqNxoCZmLxdYH2Idh9RLibH2yA=";
  }
```

Now the derivation is truly reproducible: all the inputs have been locked down
to a specific version with an associated hash for verification, and we should
get the same result no matter where we build this.

(If you try to `nix-build` now, it should finish immediately. This is because
the new derivation is semantically identical to the original one.)

This is different from most other packaging systems, including Docker, where
package installs and downloads are rarely accompanied by a checksum, making
builds non-reproducible.

## Development environments

We now know how to try out packages using `nix-shell -p PACKAGE…` and the
anatomy of a reproducible Nix derivation. What if I told you these can be
combined into a superpower, reducing the development environment setup
instructions in your projects to just a single line? All we need is the
reproducible nixpkgs above combined with a `pkgs.mkShell` function call in a
`shell.nix` file:

```nix
let
  pkgs =
    import (
      fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/0a4206a51b386e5cda731e8ac78d76ad924c7125.tar.gz";
        sha256 = "0gjlg8gwsgfm3gzwg5mzg4nh8lhlid18ls6barvahllbjp6sdc9j";
      }
    )
    {};
in
  pkgs.mkShell {
    packages = [
      # "pkgs.…" for each package you need, such as
      pkgs.bashInteractive # Instead of the OS shell
      pkgs.python311 # Python interpreter
    ];
  }
```

If we run `nix-shell` (rather than `nix-build`) in the same directory as this
file, we'll have Python 3.11 from that specific version of nixpkgs available for
the duration of the shell. This might not seem like a superpower, but consider:

- Ubuntu 22.04 doesn't have Python 3.11 in the official package repositories. So
  we'd have to install some other tool or configure a Personal Package Archive
  (PPA) to get a specific Python version.
- Even if our OS had Python 3.11, the patch version would generally be different
  from that of any other developer. Locking down nixpkgs means everybody gets
  the exact same version of every package, every time.
- This `shell.nix` is reusable on any version of Ubuntu. And any other Linux.
  And macOS. With no changes.
- Less obvious is that we could import any other nixpkgs version, and install an
  ancient version of Python on a new OS or a cutting edge Python version on an
  ancient OS. We could even import _multiple_ nixpkgs versions, if for some
  reason we want to run differently aged programs together.

At this point our development environment has a single dependency, Nix, and the
setup instructions can be a single line:

> Run `nix-shell` in the project directory, and `exit` when you're done.

Chances are, we need more stuff for our project. But since there's no way of
knowing what in advance, we'll just quickly go through some common patterns.

### Interpreter packages from nixpkgs

Python projects usually depend on more than just the standard library installed
with the interpreter. Say we use [pytest](https://pytest.org/) for the project.
A quick
[package search](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=pytest)
reveals that this is available as "python311Packages.pytest" (and others which
are irrelevant right now). Let's simply add it to our package list, so that
`shell.nix` looks like this:

```nix
# Omitted `let … in` section
  pkgs.mkShell {
    packages = [
      pkgs.bashInteractive
      pkgs.python311
      pkgs.python311Packages.pytest
    ];
  }
```

Run `nix-shell` again (exiting the Nix shell if you're already in it) to load
the new derivation. At this point we can verify that the `pytest` executable is
available:

```console
[nix-shell:~]$ pytest --version
pytest 7.2.1
```

And that the interpreter is aware of the module:

```console
[nix-shell:~]$ python -m pytest --version
pytest 7.2.1
```

We can achieve the same thing by creating a derivation which _explicitly_
combines the Python interpreter with the packages:

```nix
# Omitted `let … in` section
  pkgs.mkShell {
    packages = [
      pkgs.bashInteractive
      (
        pkgs.python311.withPackages (
          ps: [
            ps.pytest
          ]
        )
      )
    ];
  }
```

(`ps` is just an arbitrary parameter name here. We could call it
`pythonPackages` if we wanted to.)

### Custom packages

nixpkgs has over 6,000 Python 3.11 packages, but that's not everything in the
[Python Package Index](https://pypi.org/). If we want to include a package which
is not in nixpkgs, or a package version which was never in nixpkgs, or even a
package with a custom patch, we'll need to create a custom derivation. This is a
big subject, but for most Python packages it should be enough to call
`buildPythonPackage` and include the result in `withPackages`:

```nix
# Omitted `let … in` section
  pkgs.mkShell {
    packages = [
      pkgs.bashInteractive
      (
        pkgs.python311.withPackages (
          ps: [
            (
              ps.buildPythonPackage rec {
                pname = "some-package-name";
                version = "1.2.3";
                src = ps.fetchPypi {
                  inherit pname version;
                  hash = "sha256-H7y+N+9TBJW9fwjlO7g4qGmwdKD771eF7ScNNwphPrA=";
                };
                # Omitted any optional parameters
              }
            )
          ]
        )
      )
    ];
  }
```

At this point, going into more details would be off-topic for this introduction,
because each project has different dependencies. But the process is similar for
other languages. Please have a look at
[nixpkgs](https://github.com/NixOS/nixpkgs/) and the
[manual](https://nixos.org/manual/nixpkgs/).

### Pure Nix shell

Let's revert `shell.nix` to what it looked like at the start of this section, so
the only package we're installing is Python 3.11. You might've noticed that
while inside the Nix shell we could still run commands which are not declared in
the Nix shell, such as `cd`, `git` and `nix-shell`. In case of `cd` this is
because the Nix shell is a Bash shell, and `cd` is a Bash built-in command, so
it's always available. But the other two are trickier: `git` comes pre-installed
with Ubuntu, and `nix-shell` was installed as part of Nix all the way back at
[the start of this guide](#setting-up-nix-on-ubuntu).

To understand why these commands are available within the Nix shell, we need to
know a few things:

- The `PATH` variable is a `:`-separated list of directory paths.
- When running an executable like `git`, Bash will scan the list of directories
  within `$PATH` and run the first executable it finds within those directories.
- Nix also uses this mechanism to expose executables within `nix-shell`.
- The `PATH` variable is exported, meaning it is by default inherited by
  subshells.

The result is that Nix shells by default end up with both the executables from
the underlying operating system and those in the Nix shell. The Nix shell also
ends up with other exported variables, all of which introduce
non-reproducibility to the mix.

We can force the Nix shell to _not_ inherit most variables by running with the
`--pure` flag. And we can verify the difference in the available commands by
comparing the values of `$PATH`:

```console
vagrant@ubuntu-jammy:~$ nix-shell --pure --run 'echo "$PATH" | tr ":" "\n"'
/nix/store/pk3kkaafln68gzxawk57qp9m5h5285va-bash-interactive-5.2-p15/bin
/nix/store/hx2c957wpwa6i3fz0wiwhg72sfgq62k1-python3-3.11.2/bin
/nix/store/2y0q33dmn7846bpqcnfch4a0q2q6dmya-patchelf-0.15.0/bin
/nix/store/nlgyw2fv0cm8rkz8qm1jyw78vyif1bl9-gcc-wrapper-12.2.0/bin
/nix/store/aafdki1nf49k5vxq6gx2yabiybk2bjmw-gcc-12.2.0/bin
/nix/store/v1nar35045dqwf8yy572yvbbcg2w2678-glibc-2.37-8-bin/bin
/nix/store/arbxkmcgv9h8pjgj95c6d7r86yb77rl5-coreutils-9.1/bin
/nix/store/f83wjm5wpcxxbzwmr56q9iclsn0simph-binutils-wrapper-2.40/bin
/nix/store/f4qnwzv6y0nq8lix33jr5ykkyybs6fxf-binutils-2.40/bin
/nix/store/arbxkmcgv9h8pjgj95c6d7r86yb77rl5-coreutils-9.1/bin
/nix/store/j5wraaxv16fcl10x11566a3807nr4nlr-findutils-4.9.0/bin
/nix/store/q951w69v8kbdrw6shdpibnl594yfr0by-diffutils-3.9/bin
/nix/store/93z4n7zy5hwpn06279jlmak75jmq1db1-gnused-4.9/bin
/nix/store/c01b2gmx1fjjkpnvj6bxy9q49g8qkpka-gnugrep-3.7/bin
/nix/store/4rwqxm67y0zkbxjg14zl9fdxf30cpgvy-gawk-5.2.1/bin
/nix/store/lcfhnr6wrj9ssd3dxs39sprvz6qrxlj5-gnutar-1.34/bin
/nix/store/2nprqmdmjmy5i2sii7j21fznmkwimqcr-gzip-1.12/bin
/nix/store/gz0kx5v2asvlbf7gzr4v24h7dpza70zf-bzip2-1.0.8-bin/bin
/nix/store/vg9f8pmd2g0x3gb53nxwkw3yxizl3jpk-gnumake-4.4.1/bin
/nix/store/zlf0f88vj30sc7567b80l52d19pbdmy2-bash-5.2-p15/bin
/nix/store/swf1dckghdx7nza1lxz6s462pafwd7wa-patch-2.7.6/bin
/nix/store/z5818pmhspx5772s4cp6ckhwhbin2f09-xz-5.4.2-bin/bin
/nix/store/0xpv4lac3ybc6hm9gg7ywkdazs4vsj8l-file-5.44/bin
```

As we can see, the "pure" Nix shell `$PATH` only has Nix store paths. The
"impure" Nix shell has a bunch of non-Nix paths at the end:

```console
vagrant@ubuntu-jammy:~$ nix-shell --run 'echo "$PATH" | tr ":" "\n"'
/nix/store/pk3kkaafln68gzxawk57qp9m5h5285va-bash-interactive-5.2-p15/bin
/nix/store/hx2c957wpwa6i3fz0wiwhg72sfgq62k1-python3-3.11.2/bin
/nix/store/2y0q33dmn7846bpqcnfch4a0q2q6dmya-patchelf-0.15.0/bin
/nix/store/nlgyw2fv0cm8rkz8qm1jyw78vyif1bl9-gcc-wrapper-12.2.0/bin
/nix/store/aafdki1nf49k5vxq6gx2yabiybk2bjmw-gcc-12.2.0/bin
/nix/store/v1nar35045dqwf8yy572yvbbcg2w2678-glibc-2.37-8-bin/bin
/nix/store/arbxkmcgv9h8pjgj95c6d7r86yb77rl5-coreutils-9.1/bin
/nix/store/f83wjm5wpcxxbzwmr56q9iclsn0simph-binutils-wrapper-2.40/bin
/nix/store/f4qnwzv6y0nq8lix33jr5ykkyybs6fxf-binutils-2.40/bin
/nix/store/arbxkmcgv9h8pjgj95c6d7r86yb77rl5-coreutils-9.1/bin
/nix/store/j5wraaxv16fcl10x11566a3807nr4nlr-findutils-4.9.0/bin
/nix/store/q951w69v8kbdrw6shdpibnl594yfr0by-diffutils-3.9/bin
/nix/store/93z4n7zy5hwpn06279jlmak75jmq1db1-gnused-4.9/bin
/nix/store/c01b2gmx1fjjkpnvj6bxy9q49g8qkpka-gnugrep-3.7/bin
/nix/store/4rwqxm67y0zkbxjg14zl9fdxf30cpgvy-gawk-5.2.1/bin
/nix/store/lcfhnr6wrj9ssd3dxs39sprvz6qrxlj5-gnutar-1.34/bin
/nix/store/2nprqmdmjmy5i2sii7j21fznmkwimqcr-gzip-1.12/bin
/nix/store/gz0kx5v2asvlbf7gzr4v24h7dpza70zf-bzip2-1.0.8-bin/bin
/nix/store/vg9f8pmd2g0x3gb53nxwkw3yxizl3jpk-gnumake-4.4.1/bin
/nix/store/zlf0f88vj30sc7567b80l52d19pbdmy2-bash-5.2-p15/bin
/nix/store/swf1dckghdx7nza1lxz6s462pafwd7wa-patch-2.7.6/bin
/nix/store/z5818pmhspx5772s4cp6ckhwhbin2f09-xz-5.4.2-bin/bin
/nix/store/0xpv4lac3ybc6hm9gg7ywkdazs4vsj8l-file-5.44/bin
/home/vagrant/.nix-profile/bin
/nix/var/nix/profiles/default/bin
/usr/local/sbin
/usr/local/bin
/usr/sbin
/usr/bin
/sbin
/bin
/usr/games
/usr/local/games
/snap/bin
```

Since the Nix store paths are at the start, the executables there get higher
precedence, so often this doesn't matter. But if we forget to include a package
we depend on in the Nix shell, and that package happens to be installed in the
underlying operating system, we've introduced non-reproducibility into the
process. So it's a good idea to use pure Nix shells for example in automation,
to make sure things work the same as on developer machines.

As an aside, when running pure Nix shells you might still want some developer
tools like Git with all the bells and whistles, so you probably want
`pkgs.gitFull` in your package list. And if you need to do any TLS certificate
validation (basically, fetching any HTTPS URLs) you'll want `pkgs.cacert`, the
index of trusted TLS certificate authorities (CAs).

### Changing shells

Since each Nix shell is basically just a Bash shell with some extra context,
changing development environments is as easy as exiting the current shell
(running `exit` or pressing <kbd>Ctrl+d</kbd>), `cd`-ing to another project
directory, and running `nix-shell` again.

If you change Nix shells often, you probably want to set up
[nix-direnv](https://github.com/nix-community/nix-direnv)[^nix-direnv-setup]. It
has a few advantages over vanilla Nix shells, and works seamlessly alongside
`nix-shell`. Basically, with just a `.envrc` file containing `use nix` we
activate and deactivate Nix shells by simply `cd`-ing into and out of the
project directory.

[^nix-direnv-setup]:
    I'd recommend
    [centralising the direnv cache](https://github.com/direnv/direnv/wiki/Customizing-cache-location),
    to avoid having to deal with an unwanted `.direnv` directory in each
    repository.

## Gotchas

Nix is only supported on Linux and macOS, because it relies heavily on POSIX
functionality. You will need Windows Subsystem for Linux (WSL) if you want to
run it on Windows.

When writing scripts for use with Nix, make sure to use
[portable shebang lines](<https://en.wikipedia.org/w/index.php?title=Shebang_(Unix)&oldid=878552871#Portability>).
That's the line at the start of scripts starting with `#!`, which specifies the
interpreter to use for the file. Nix doesn't pollute the global namespace, but
instead modifies the `PATH` variable to make executables available to you. So if
your script contains a shebang line starting with, for example,
`#!/usr/bin/python` or `#!/bin/bash`, it's not going to use the interpreter
installed within your Nix environment. Thankfully there's a POSIX-compliant way
to do this, which is also best practice when not using Nix: use
`#!/usr/bin/env EXECUTABLE`, for example `#!/usr/bin/env python`. This will use
the first matching executable on your `$PATH`.

It's not always easy to find existing functionality. Core functions are usually
[builtins](https://nixos.org/manual/nix/stable/language/builtins.html), but
there are a lot of useful functions implemented inside
[nixpkgs](https://github.com/NixOS/nixpkgs/)'s `lib` and `stdenv` attributes.

IDE support is still behind popular general-purpose languages. You might want to
check out plugins for your favourite IDE.

You are likely to encounter suggestions to use the _`nix` command_ or something
called _flakes_. Both of these are useful additions to the Nix ecosystem, but
are currently considered experimental features. I won't go through them in more
detail here, since they might change, but feel free to
[enable the `nix` command](https://nixos.wiki/wiki/Nix_command#Enabling_the_nix_command)
or [flakes](https://nixos.wiki/wiki/Flakes#Enable_flakes) if you're comfortable
with that.

## Summary

Nix is mature. Development started in 2003, and as of 27 April 2023:

- [Nixpkgs 22.11 provides 86,044 packages](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=+),
  compared to [74,279 for Ubuntu 22.04](https://askubuntu.com/a/1006766/10371)
  and [6,639 for Brew](https://formulae.brew.sh/api/formula.json).
- There are 5,213 all-time
  [contributors to the package repository](https://github.com/NixOS/nixpkgs),
  with 233 in [the last month](https://github.com/NixOS/nixpkgs/pulse).
- Nix is [sponsored](https://opencollective.com/nixos) by 488 individuals and
  companies.
- There are 738
  [GitHub organisation members](https://github.com/orgs/NixOS/people).
- The [community](https://nixos.org/community/) is present on all the major
  platforms.
- There's a yearly [NixCon](https://nixcon.org/) conference.
- [Lots of companies provide commercial support](https://nixos.org/community/commercial-support.html).
- Nix is used by many big companies, including
  [Mozilla](https://github.com/mozilla/nixpkgs-mozilla),
  [Target](https://github.com/target/nix-fetchers), and
  [more](#success-stories).

Nix improves on traditional packaging tools and installers in many ways:

- Builds are reproducible by default. With other packaging tools, like dpkg or
  Docker, you need to do a lot of extra non-trivial work to have a truly
  reproducible build. This applies just as much to packages as to development
  environments.
- The default developer use case is super simple: run `nix-shell` in a project
  directory to make all the tools you need for development available in the
  shell. Without Nix you typically need to first install a "version switcher"
  software like NVM or pyenv, then install the interpreter, then a package
  manager, then the packages, and then possibly even configure your shell.
- Working with multiple projects is trivial and risk-free, since packages are
  all compartmentalised.
- Packaging is much simpler than in many other packaging systems, like dpkg.
- Docker images produced by Nix are by default minimal. With Dockerfiles, by
  contrast, it's necessary to jump through hoops to avoid including package
  managers, build tools and the like in the final image.
- [Free 5 GB package cache](https://www.cachix.org/) with easy
  setup[^cache-setup], funded by donations
  ([Toitū Te Whenua LINZ is already using this](https://app.cachix.org/cache/linz)
  for CI).
- In my opinion it is _vastly_ easier to
  [contribute to nixpkgs](https://github.com/NixOS/nixpkgs/blob/1aa3393fced9720eb7d18888d4a0fce241dd4ff0/CONTRIBUTING.md)
  than, say, [Ubuntu](https://packaging.ubuntu.com/html/). In less than two
  years I've had
  [26 PRs accepted to nixpkgs](https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3Al0b0+is%3Amerged),
  including new packages, metadata fixes, lint cleanup, remote build changes,
  and tests.

[^cache-setup]:
    1. Log in using a GitHub account.
    1. Create a uniquely named cache.
    1. Create a key in Settings → Auth Tokens.
    1. Use a
       [GitHub Action](https://github.com/linz/emergency-management-tools/blob/593f6f893cd1a89843f93ca3c816be35c47c1a60/.github/workflows/test.yml#L18-L21)
       or
       [a few lines of Bash](https://gitlab.com/engmark/mypy-exercises/blob/2b3f434ca9e63d69ced450d68e85f072671b5dc1/.gitlab-ci.yml#L19-37)
       to do the pulling and pushing.

## Links

### Starting points

- [Homepage](https://nixos.org)
- [Manual](https://nixos.org/manual/nix/stable/)
- [The Purely Functional Software Deployment Model](https://edolstra.github.io/pubs/phd-thesis.pdf)
  outlines the original ideas behind Nix

### Community

- [Forum](https://discourse.nixos.org/)
- [Live chat](https://matrix.to/#/#community:nixos.org)

### Repos

- [Package manager](https://github.com/NixOS/nix)
- [Packages](https://github.com/NixOS/nixpkgs)

### Tools

- [Dead code finder](https://github.com/astro/deadnix/)
- [Find all versions of a package](https://lazamar.co.uk/nix-versions/)
- [Formatter](https://kamadorueda.com/alejandra/)
- [Linter](https://git.peppe.rs/languages/statix/about/)
- [Official Docker image](https://hub.docker.com/r/nixos/nix)

### Other intros

- [What Nix Can Do (Docker Can't)](https://youtu.be/6Le0IbPRzOE)
- [Developer guide](https://nix.dev/), a "guide for developers getting things
  done using the Nix ecosystem"
- [Getting Started with Nix](https://youtu.be/xXlCcdPz6Vc?t=68) showcases
  installing packages and packaging using flakes
- [How I Start: Nix](https://xeiaso.net/blog/how-i-start-nix-2020-03-08), on
  bootstrapping a Rust project using Nix
- [How do Nix builds work?](https://jvns.ca/blog/2023/03/03/how-do-nix-builds-work-/)
  looks under the hood at how to "compile a C program, without using Nix’s
  standard machinery"
- [Nix Pills](https://nixos.org/guides/nix-pills/) "provides a tutorial
  introduction into the Nix package manager and Nixpkgs package collection"
- [Nix Turns 20. What the Hell Is It?](https://earthly.dev/blog/what-is-nix/)
  introduces Nix concepts
- [Nix flakes](https://www.tweag.io/blog/2020-05-25-flakes/) explains this new
  feature
- [Nix – taming Unix with functional programming](https://www.tweag.io/blog/2022-07-14-taming-unix-with-nix/)
- [Zero to Nix](https://zero-to-nix.com/), "[a]n unofficial, opinionated, gentle
  introduction to Nix"

### Success stories

- [A success story of adopting Nix at a workplace](https://fosdem.org/2023/schedule/event/nix_and_nixos_a_success_story/)
- [Building 15-year-old software with Nix](https://blinry.org/nix-time-travel/)
- [How Shopify Uses Nix](https://youtu.be/KaIRpx11qrc)
- [How we went from supporting 50 languages to all of them](https://blog.replit.com/nix)
- [I was Wrong about Nix](https://xeiaso.net/blog/i-was-wrong-about-nix-2020-02-10),
  about moving from Docker to Nix
- [Nix on the Steam Deck](https://determinate.systems/posts/nix-on-the-steam-deck)
- [Playing with Nix in adverse HPC environments](https://fosdem.org/2023/schedule/event/nix_and_nixos_playing_with_nix_in_hpc_environments/)
- [Taking off with Nix at FlightAware](https://flightaware.engineering/taking-off-with-nix-at-flightaware/)

### Examples

- [Simple Nix shell](https://gitlab.com/engmark/tilde/-/blob/7d529258e67a23768cc850d165d02ec235b6c649/shell.nix)
- [Nix shell with locale support](https://gitlab.com/engmark/engmark.gitlab.io/-/blob/0b0065b2737459b44405712a5180c370304a4811/shell.nix)
- [Jupyter + Node.js shell](https://github.com/linz/stac/blob/33148392e64c61ad37c8b131a2e28c3d7066091a/shell.nix)
  with manual pinning
- [Python development with or without Nix](https://gitlab.com/engmark/mypy-exercises/-/tree/2b3f434ca9e63d69ced450d68e85f072671b5dc1#install)
  using [poetry2nix](https://github.com/nix-community/poetry2nix/)
- [Subproject Nix shells](https://github.com/linz/emergency-management-tools/blob/36018599da5a7af5a6dbfa4d8e4f6cc946c8b990/shell.nix)
- [Shell including a Python package build](https://gitlab.com/engmark/vcard/-/tree/0a1ec53581207eb8412c0efa4cb0b64cba106ed1)

## Vocabulary

- An _attribute set_ is a key/value mapping, equivalent to a Python `dict` or a
  JSON object.
- A _derivation_ is a build task, that is, the Nix code used to build something.
  For example, we
  [built the GNU Hello package derivation](#building-gnu-hello-with-nix) above.
  Usually created with the `mkDerivation` function or one of its many wrappers.
- The _Nix store_ refers to the directory where derivation results are stored,
  usually `/nix/store`.
- _"Package"_ is usually used for any derivation which creates files in the Nix
  store.

## Acknowledgements

Huge thanks to [Toitū Te Whenua LINZ](https://www.linz.govt.nz/) for letting me
work on this.

## Copyright

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")

This content is copyright © 2023 Victor Engmark, and licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
A copy of the license is provided in the [LICENSE](LICENSE) file.
