let
  pkgs = import (builtins.fetchTarball {
    name = "nixos-unstable-2024-11-27";
    url = "https://github.com/nixos/nixpkgs/archive/4633a7c72337ea8fd23a4f2ba3972865e3ec685d.tar.gz";
    sha256 = "0z9jlamk8krq097a375qqhyj7ljzb6nlqh652rl4s00p2mf60f6r";
  }) { };

  mkKernelSpec =
    ps: spec:
    ps.buildPythonPackage {
      name = spec.name + "-spec";
      format = "other";
      dontUnpack = true;
      dontConfigure = true;
      dontBuild = true;
      installPhase = ''
        mkdir -p $out/share/jupyter/kernels/${spec.name}
        cat > $out/share/jupyter/kernels/${spec.name}/kernel.json << 'EOF'
        ${builtins.toJSON spec}
        EOF
      '';
    };

  pythonEnv = pkgs.python3.withPackages (ps: [
    ps.jupyter-client
    ps.nix-kernel
    (mkKernelSpec ps {
      name = "nix-kernel";
      display_name = "Nix kernel";
      argv = [
        "python"
        "-m"
        "nix-kernel"
        "-f"
        "{connection_file}"
      ];
    })
  ]);
in
pkgs.mkShell {
  packages = [
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.deadnix
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixfmt-rfc-style
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.statix
    pythonEnv
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
}
